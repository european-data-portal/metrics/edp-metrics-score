# ChangeLog

## Unreleased

## [1.0.2](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-metrics-score/tags/1.0.2) (2020-03-11)

**Changed:**
* Explicitly close dataset

## [1.0.1](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-metrics-score/tags/1.0.1) (2020-03-08)

**Fixed:**
* Scoring only best urls, not all

## [1.0.0](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-metrics-score/tags/1.0.0) (2020-02-28)

Initial production release

**Added:**
* `PV.formatMatch` and `PV.syntaxValid` scoring
* Default score values separated from main vocabulary
* Load score values from file, otherwise load default score values
 
**Changed**
* Score is stored as integer and not as concept anymore
* Score only best distribution 

**Fixed:**
* Reuse one single web client in verticle
* Score calculation for status codes
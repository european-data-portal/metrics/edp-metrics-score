package io.piveau

import io.piveau.pipe.connector.PipeConnector
import io.piveau.metrics.MetricsScoreVerticle
import io.vertx.core.DeploymentOptions
import io.vertx.core.Launcher
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.awaitResult

class MainVerticle : CoroutineVerticle() {

    override suspend fun start() {
        awaitResult<String> { vertx.deployVerticle(
            MetricsScoreVerticle::class.java, DeploymentOptions()
            .setWorker(true), it) }
        awaitResult<PipeConnector> { PipeConnector.create(vertx, DeploymentOptions(), it) }.consumer(MetricsScoreVerticle.ADDRESS)
    }

}

fun main(args: Array<String>) {
    Launcher.executeCommand("run", *(args.plus(MainVerticle::class.java.name)))
}

package io.piveau.metrics

import io.piveau.dqv.listMetricsModels
import io.piveau.pipe.PipeContext
import io.piveau.rdf.RDFMimeTypes
import io.piveau.rdf.asRdfLang
import io.piveau.rdf.asString
import io.piveau.rdf.toDataset
import io.piveau.vocabularies.PiveauScoring
import io.vertx.core.eventbus.Message
import io.vertx.kotlin.core.file.readFileAwait
import io.vertx.kotlin.coroutines.CoroutineVerticle
import org.apache.jena.riot.Lang
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.RDF
import org.slf4j.LoggerFactory
import java.io.ByteArrayInputStream

class MetricsScoreVerticle : CoroutineVerticle() {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    override suspend fun start() {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe)

        try {
            vertx.fileSystem().readFileAwait("config/piveau-dqv-vocabulary-score-values.ttl").also {
                logger.info("Loaded score values from file")
            }
        } catch (e: Exception) {
            vertx.fileSystem().readFileAwait("piveau-dqv-vocabulary-default-score-values.ttl").also {
                logger.info("Loaded default score values")
            }
        }.apply {
            PiveauDQVVocabulary.loadScoreValues(ByteArrayInputStream(bytes))
        }
    }

    private fun handlePipe(message: Message<PipeContext>): Unit = with(message.body()) {
        if (config.getBoolean("skip", false)) {
            pass().also { log.debug("Data scoring skipped: {}", dataInfo) }
        } else {
            val content = if (pipeManager.isBase64Payload) binaryData else stringData.toByteArray()

            with(content.toDataset(mimeType?.asRdfLang() ?: Lang.TRIG)) {
                val metricsModel = listMetricsModels().first()
                val resource = defaultModel.listSubjectsWithProperty(RDF.type, DCAT.Dataset).next()
                val score = scoreMetrics(metricsModel, resource)
                log.info(
                    "Dataset scored as {} ({}): {}",
                    PiveauScoring.getConcept(PiveauScoring.abstract(score))?.label("en") ?: "nothing",
                    score,
                    dataInfo
                )
                log.debug("Dataset content: {}", asString())
                setResult(asString(), RDFMimeTypes.TRIG, dataInfo).forward()
                close()
            }
        }
    }

    companion object {
        const val ADDRESS: String = "io.piveau.pipe.metrics.score.queue"
    }

}

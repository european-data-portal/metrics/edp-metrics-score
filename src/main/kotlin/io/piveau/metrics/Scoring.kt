package io.piveau.metrics

import io.piveau.dqv.addMeasurement
import io.piveau.vocabularies.vocabulary.DQV
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCAT

fun scoreMetrics(metrics: Model, resource: Resource): Int {

    val distributions = resource.listProperties(DCAT.distribution).mapWith { it.resource }.toList()
    var score = distributions.map { dist ->

        val accessUrlStatusCodes = metrics.listObjectsOfProperty(dist, DQV.hasQualityMeasurement)
            .filterKeep {
                it.asResource().hasProperty(DQV.isMeasurementOf, PV.accessUrlStatusCode)
            }.mapWith { it.asResource() }.toList()

        var s = accessUrlStatusCodes.map { it.score() }.max() ?: 0

        val downloadUrlStatusCodes = metrics.listObjectsOfProperty(dist, DQV.hasQualityMeasurement)
            .filterKeep {
                it.asResource().hasProperty(DQV.isMeasurementOf, PV.downloadUrlStatusCode)
            }.mapWith { it.asResource() }.toList()

        s += downloadUrlStatusCodes.map { it.score() }.max() ?: 0

        s += metrics.listObjectsOfProperty(dist, DQV.hasQualityMeasurement)
            .filterDrop { accessUrlStatusCodes.contains(it) || downloadUrlStatusCodes.contains(it) }
            .mapWith { it.asResource() }
            .asSequence().sumBy { it.score() }

        s
    }.max() ?: 0

    score += metrics.listSubjectsWithProperty(DQV.hasQualityMeasurement)
        .filterDrop { distributions.contains(it) }.asSequence()
        .flatMap {
            it.listProperties(DQV.hasQualityMeasurement).asSequence()
        }.map { it.resource }.sumBy { it.score() }

    metrics.addMeasurement(resource, PV.scoring, score)

    return score
}

fun Resource.score(): Int = getProperty(DQV.isMeasurementOf)?.resource?.let {
    when (it) {
        PV.keywordAvailability -> it.booleanScore(this)
        PV.categoryAvailability -> it.booleanScore(this)
        PV.spatialAvailability -> it.booleanScore(this)
        PV.temporalAvailability -> it.booleanScore(this)
        PV.accessUrlStatusCode -> PiveauDQVVocabulary.metric(it.uri).let { res ->
            if (getProperty(DQV.value).int in (200..299)) res.getProperty(PV.trueScore).int else res.getProperty(PV.falseScore)?.int
                ?: 0
        }
        PV.downloadUrlAvailability -> it.booleanScore(this)
        PV.downloadUrlStatusCode -> PiveauDQVVocabulary.metric(it.uri).let { res ->
            if (getProperty(DQV.value).int in (200..299)) res.getProperty(PV.trueScore).int else res.getProperty(PV.falseScore)?.int
                ?: 0
        }
        PV.formatAvailability -> it.booleanScore(this)
        PV.mediaTypeAvailability -> it.booleanScore(this)
        PV.formatMediaTypeVocabularyAlignment -> it.booleanScore(this)
        PV.formatMediaTypeNonProprietary -> it.booleanScore(this)
        PV.formatMediaTypeMachineInterpretable -> it.booleanScore(this)
        PV.dcatApCompliance -> it.booleanScore(this)
        PV.formatMatch -> it.booleanScore(this)
        PV.syntaxValid -> it.booleanScore(this)
        PV.licenceAvailability -> it.booleanScore(this)
        PV.knownLicence -> it.booleanScore(this)
        PV.accessRightsAvailability -> it.booleanScore(this)
        PV.accessRightsVocabularyAlignment -> it.booleanScore(this)
        PV.contactPointAvailability -> it.booleanScore(this)
        PV.publisherAvailability -> it.booleanScore(this)
        PV.rightsAvailability -> it.booleanScore(this)
        PV.byteSizeAvailability -> it.booleanScore(this)
        PV.dateIssuedAvailability -> it.booleanScore(this)
        PV.dateModifiedAvailability -> it.booleanScore(this)
        PV.atLeastFourStars -> it.booleanScore(this)
        else -> 0
    }
} ?: 0

fun Resource.booleanScore(resource: Resource): Int = PiveauDQVVocabulary.metric(uri).let {
    if (resource.getProperty(DQV.value).boolean) it.getProperty(PV.trueScore)?.int
        ?: 0 else it.getProperty(PV.falseScore)?.int ?: 0
}
